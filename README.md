# Form Builder 
There are two tabs "Config" and "Result" on the screen.
 Example JSON-file: /public/data/example.json

## How to run
* Install [node](https://nodejs.org/en/) and [yarn](https://yarnpkg.com/en/)
* Run `yarn start`

| Params types | Description | Default value |
| ---       | --- | --- |
| label     | caption of the control | label |
| hint      | hint shown below the control | `empty` |
| value     | the actual value | `empty` |
| min       | minimum value allowed for number drop-down | 0 |
| max       | maximum value allowed for number drop-down | 100 |
| checked   | checkbox is pre-selected or not | false |
| options   | comma separated values | n/a |
|           | `All parameters are defined as strings.` | |


| Field types | Params |
| ---         | --- |
| Text | label, hint, value |
| Number | label, min, max, step, value | 
| Textarea | label, hint, value |
| Checkbox | label, checked |
| Date | label, value |
| Radio | label, options, value |
| Buttons | label, options |