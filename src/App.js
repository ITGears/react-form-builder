import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Container from 'react-bootstrap/Container';
import Jumbotron from "react-bootstrap/Jumbotron";

import FormBuilderComponent from './components/FormBuilderComponent';


function App() {
  return (
    <div className="App">
		<Container className="p-3">
		<Jumbotron>
			<FormBuilderComponent />
		</Jumbotron>
		</Container>
	</div>
  );
}

export default App;
