import React, { Component } from 'react';

import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Button from 'react-bootstrap/Button';

import axios from 'axios';

import TextFieldComponent from './controls/TextFieldComponent';
import NumberFieldComponent from './controls/NumberFieldComponent';
import TextareaFieldComponent from './controls/TextareaFieldComponent';
import CheckboxFieldComponent from './controls/CheckboxFieldComponent';
import DateFieldComponent from './controls/DateFieldComponent';
import RadioFieldComponent from './controls/RadioFieldComponent';
import ButtonsFieldComponent from './controls/ButtonsFieldComponent';

class FormBuilderComponent extends Component {
	
	allFields = {
		text: TextFieldComponent,
		number: NumberFieldComponent,
		textarea: TextareaFieldComponent,
		checkbox: CheckboxFieldComponent,
		date: DateFieldComponent,
		radio: RadioFieldComponent,
		buttons: ButtonsFieldComponent
	}

	constructor(props) {
		super(props);
		this.state = { 
			activeTab: 'tab_config',
			formTitle: '',
			formFieldsJSON: '', 
			formFields: 'No results generated yet.',
			'parser_error': '',
		};
	}


	componentDidMount() {
		axios.get('../data/example.json')
			.then(response => {
				this.setState({'formTitle': response.data.title});
				this.setState({ formFieldsJSON: JSON.stringify(response.data, null, 2) });
			});
	}


	tabChanged(event) {
		this.setState({activeTab: event});
	}


	isValidJSONString = (str) => {
		try {
			JSON.parse(str);
		} catch (e) {
			return false;
		}
		return true;
	}

	
	textareaChanged(event) {
		this.setState({formFieldsJSON: event.target.value});
	}

	
	generateFields = () => {
		if (this.isValidJSONString(this.state.formFieldsJSON) === false) {
			this.setState({'parser_error': 'Not valid JSON format!'});
			return;
		} 
		this.setState({'parser_error': ''});

		const data = JSON.parse(this.state.formFieldsJSON);
		this.setState({'formTitle': data.title});
		this.renderFields();
		this.setState({activeTab: 'tab_result'}); // switch into Result tab
	}

	renderFields() {
		if (this.state.formFieldsJSON === '') return;
		let form = Object.values(JSON.parse(this.state.formFieldsJSON).items).map((fieldData, index) => {
			const FieldComponent = this.allFields[fieldData.type] || "__undef";
			if (FieldComponent !== "__undef") {
				return (
					<div key={"i" + index} className="form-group">
						<FieldComponent class="form-input" type="type" {...fieldData} />
		 			</div>
				 );
			} else {
				console.warn('Undefined component type: ', fieldData.type);
			return (null);
			}
		});
		return form;
	}


	render() {
		return (
			<Tabs defaultActiveKey="tab_config" activeKey={this.state.activeTab} onSelect={(e) => this.tabChanged(e)}>
				<Tab eventKey="tab_config" title="Config">
					<textarea id="textareaConfig" rows="10" value={this.state.formFieldsJSON} onChange={(e) => this.textareaChanged(e)}></textarea>
					<Button variant="primary" onClick={this.generateFields}>Generate!</Button> <span className="parser_error">{this.state.parser_error}</span>
				</Tab>
				<Tab eventKey="tab_result" title="Result">
					<h1>{this.state.formTitle}</h1>
					{this.renderFields()}
				</Tab>
			</Tabs>
		);
	}

}

export default FormBuilderComponent;