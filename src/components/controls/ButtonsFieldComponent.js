import React, { Component } from 'react';
import Button from '@material-ui/core/Button';

class ButtonsFieldComponent extends Component {

	render() {
		const { options='n/a' } = this.props;
		const buttons = options.split("|").map((value, index) => {
			return (
				<span key={"i" + index} className="form-group buttons-set">
				<Button variant="contained" color="primary">{value}</Button>
				</span>
			);
		})

		return (
			<div>
				{buttons}
			</div>
		);
	}
}

export default ButtonsFieldComponent;