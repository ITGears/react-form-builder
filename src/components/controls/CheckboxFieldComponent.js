import React, { Component } from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

class CheckboxFieldComponent extends Component {

	render() {
		const { label='label', checked=true } = this.props;
		return (
			<FormControlLabel
			control={
				<Checkbox 
					checked={checked} 
					color="primary" 
				/>
			}
			label={label}
			/>
		);
	}
}

export default CheckboxFieldComponent;