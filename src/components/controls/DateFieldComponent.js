import React, { Component } from 'react';
import DateFnsUtils from "@date-io/date-fns";
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";

class DateFieldComponent extends Component {

	handleChange(event) {
		console.log('new date', event);
	}

	render() {
		const { label='label', value='' } = this.props;
		return (
			<MuiPickersUtilsProvider utils={DateFnsUtils}>
				<DatePicker 
					label={label} 
					value={value || "2010-12-12"}
					onChange={(e) => this.handleChange(e)}  
				/>
			</MuiPickersUtilsProvider>
		);
	}
}

export default DateFieldComponent;