import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';

class NumberFieldComponent extends Component {

	render() {
		const { label='label', min=0, max=100, step=1, value=0 } = this.props;
		return (
			<TextField type="number" 
				inputProps={{min: min, max: max, step: step}} 
				label={label} 
				value={value} 
				margin="normal" />
		);
	}
}

export default NumberFieldComponent;