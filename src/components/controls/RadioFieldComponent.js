import React, { Component} from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormLabel from '@material-ui/core/FormLabel';

class RadioFieldComponent extends Component {

	handleChange(event) {
		console.log('new radio value: ', event.target.value);
	}


	render() {
		const { label='label', options='n/a', value = '' } = this.props;
		const input_options = options.split("|").map((o_value, idx) => {
			return (
				<FormControlLabel 
					key={idx} value={o_value} 
					checked={o_value === value} 
					control={<Radio />} 
					label={o_value} />
			);
		})

		return (
			<div>
				<FormLabel component="legend">{label}</FormLabel>
				<RadioGroup row onChange={(e) => this.handleChange(e)}>
					{input_options}
				</RadioGroup>
			</div>
		);
	}
}

export default RadioFieldComponent;