import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldComponent extends Component {

	render() {
		const { label='label', hint='', value='' } = this.props;
		return (
			<TextField 
				label={label} 
				value={value}
				helperText={hint} 
				margin="normal" />
		);
	}
}

export default TextFieldComponent;