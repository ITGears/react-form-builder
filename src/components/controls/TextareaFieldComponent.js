import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';

class TextareaFieldComponent extends Component {

	render() {
		const { label='label', hint='', value='' } = this.props;
		return (
			<TextField multiline rows="4" 
				label={label} 
				value={value} 
				helperText={hint} 
				margin="normal" />
		);
	}
}

export default TextareaFieldComponent;